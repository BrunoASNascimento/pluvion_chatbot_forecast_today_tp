def get_json(value, info_str, icon, unit):
    data = {
        "status": True,
        "value": round(value, 1),
        "unit": unit,
        "info_str": info_str,
        "icon": icon
    }

    return data


def arh(value):
     # umidade
    # tabela de acordo com o site CGESP.ORG para os estados de atencao, alerta e emergencia
    # o range ideal e de acordo com a recomendacao da OMS
    # os ranges que sobraram foram classificados em "baixa" e "alta"
    if value <= 12:
        info_str = "muito baixa - Estado de Emergência"
        icon = " 🥵"
    elif value > 12 and value <= 20:
        info_str = "muito baixa - Estado de Alerta"
        icon = " 🥵"
    elif value > 20 and value <= 30:
        info_str = "baixa - Estado de Atenção"
        icon = " 💧"
    elif value > 30 and value <= 50:
        info_str = "baixa"
        icon = " 💧"
    elif value > 50 and value <= 80:
        info_str = "ideal - sem problemas 🙃"
        icon = " 💧💧"
    elif value > 80:
        info_str = "alta"
        icon = " 💧💧💧"
    unit = "%"
    data_json = get_json(value, info_str, icon, unit)

    return data_json


def pc_int(value):

    if value < 0.25:
        info_str = "sem chuva"
        icon = " ☁ 😎"
    elif value >= 0.25 and value < 2.5:
        info_str = "chuva fraca"
        icon = " 🌦 um par de galochas evitará que molhe as meias 😄"
    elif value >= 2.5 and value < 7.6:
        info_str = "chuva moderada"
        icon = " 🌧 é bom levar o ☂"
    elif value >= 7.5 and value < 11:
        info_str = "chuva forte"
        icon = " 🌧 atenção ao sair ☂"
    elif value >= 11 and value < 15:
        info_str = "chuva muito forte"
        icon = " 🌧 atenção ao sair ☂"
    elif value >= 15:
        info_str = "chuva extremamente forte"
        icon = " ⛈ possibilidade de alagamentos - se mantenha em lugar seguro"

    unit = "mm/h"
    data_json = get_json(value, info_str, icon, unit)

    return data_json


def pc_vol(value):
    if value < 0.26:
        info_str = "sem chuva"
        icon = "☁"
    elif value >= 0.26 and value < 3:
        info_str = "pouca chuva"
        icon = "🌦"
    elif value >= 3 and value < 10:
        info_str = "chuva moderada"
        icon = "🌧"
    elif value >= 10 and value < 50:
        info_str = "bastante chuva"
        icon = "🌧"
    elif value >= 50:
        info_str = "muita chuva"
        icon = "⛈"

    unit = "mm"
    data_json = get_json(value, info_str, icon, unit)

    return data_json


def tp(value):
    if (value < 12):
        info_str = "muito frio"
        icon = " ⛇ lembre do agasalho e, se possível, ajude quem não tem um 🤒"
    elif (value >= 12 and value < 16):
        info_str = "bem frio"
        icon = " 🥶 não esquece o casaquinho 🧥"
    elif (value >= 16 and value < 20):
        info_str = "friozinho"
        icon = " 🥶 mas ainda dá pra sair sem blusa 🙃"
    elif (value >= 20 and value < 25):
        info_str = "ameno"
        icon = " 🌤"
    elif (value >= 25 and value < 30):
        info_str = "calor"
        icon = " ☀ lembre de tomar bastante água 💦"
    elif (value >= 30):
        info_str = "muito calor"
        icon = " 🥵 Hidrate-se bem e lembre de passar o protetor solar"
    unit = "°C"
    data_json = get_json(value, info_str, icon, unit)

    return data_json


def ws(value):
    # Escala de Beaufort SIMPLIFICADA, e utilizando a media dos valores de vento considerados
    if value <= 0.3:
        info_str = ": brisa leve"
        icon = " 💨"
    elif value > 0.3 and value <= 3.3:
        info_str = "suave"
        icon = " 💨 só sentar e aproveitar o vento no rosto"
    elif value > 3.3 and value <= 7.9:
        info_str = "moderado"
        icon = " 💨💨"
    elif value > 7.9 and value <= 10.7:
        info_str = "forte"
        icon = " 💨💨💨"
    elif value > 10.7 and value <= 17.1:
        info_str = "forte"
        icon = " 💨💨💨"
    elif value > 17.1 and value <= 24.4:
        info_str = "muito forte"
        icon = " 💨💨💨💨"
    elif value > 24.4 and value <= 32.6:
        info_str = "massivo - cuidado ao sair"
        icon = " 💨💨💨💨💨"
    elif value > 32.6:
        info_str = ": furacão - busque abrigo e proteção"
        icon = " 💨💨💨💨💨🌪"

    value = (value*3.6)
    unit = "km/h"
    data_json = get_json(value, info_str, icon, unit)

    return data_json
